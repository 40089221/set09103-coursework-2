from flask import Flask, url_for, abort , redirect ,render_template,Markup,g,request, session
from datetime import datetime
import sqlite3

app = Flask(__name__)
db_location = 'ds/bbsdb.db'

app.secret_key = ']rg5fW0Wt[rk2@s~f}+-84.Q!#+5$'

def get_db():
  db = getattr(g, 'db', None)
  if db is None:
    db = sqlite3.connect(db_location)
    g.db = db
  return db

@app.teardown_appcontext
def close_db_connection(exception):
  db = getattr(g, 'db', None)
  if db is not None:
    db.close()

def init_db():
  with app.app_context():
    print "DB creating..."
    db = get_db()
    db.execute('''CREATE TABLE posts (title text, url text, user text, time text)''')
    db.execute('''CREATE TABLE login (username text, password text)''')
    db.commit()
    print "DB created!"

def init_sesh(username):
  session['username'] = username
  print "session initialized"

def end_sesh():
  session.pop('username', None)

@app.route("/home/", methods=['GET','POST'])
def home():
  db = get_db()
  try:
    user = session['username']
    username = "Hello, " + user + ". To log out go to the account page. To submit a link go to the submit page."
    print "user is logged in"
   # sql = "SELECT title FROM posts WHERE user like " + user
  except:
    username = "Log in to see your latest posts here"
  page = ""
  sql = "SELECT title, url, user, time FROM posts ORDER BY time DESC"
  for row in db.cursor().execute(sql):
    page = page + "<div class = 'encapslink'>"
    var1 = row[0]
    var2 = row[1]
    var3 = row[2]
    var4 = row[3]
    print var1
    print var2
    print var3
    print var4
    html1 = "<a href = '" + var2 + "'>" + var1 + "</a>"
    html2 = "<p>" + var3 + "</p>"
    html3 = "<p>" + var4 + "</p>"
    page = page + html1
    page = page + html2
    page = page + html3
    print page
    page = page + "</div>"
  page = Markup(page)

  return render_template('home.html', page = page, username = username)

@app.route("/submit/", methods= ['GET','POST'])
def submit():
  db = get_db()
  try:
    username = session['username']
    print "user is logged in"
  except:
    return redirect(url_for('accountlogin'))

  if request.method == 'POST':
    title = request.form['title']
    url = request.form['url']
    time = datetime.now()
    time = str(time)
    print title
    print url
    print time
    sql = "insert into posts values('" + title + "','" + url + "','" +username+ "','" + time + "')"
    db.cursor().execute(sql)
    db.commit()
    for row in db.cursor().execute("SELECT rowid, * FROM posts ORDER BY rowid"):
      print(str(row))
  return render_template('post.html')

@app.route("/account/", methods=['GET', 'POST'])
def accountCreate():
  db = get_db()
  try:
    username = session['username']
    return redirect(url_for('myAccount'))
  except:
    if request.method == 'POST':
      username = request.form['username']
      password = request.form['password']
      print username
      print password
      sql = "insert into login values ('" + username + "','" + password + "')"
      db.cursor().execute(sql)
      db.commit()
      init_sesh(username = username)
      for row in db.cursor().execute("SELECT rowid, * FROM login ORDER BY username"):
        print(str(row))
      return redirect(url_for('myAccount'))
  return render_template('account.html')


@app.route("/login/", methods=['GET', 'POST'])
def accountlogin():
  db= get_db()
  try:
   username = session['username']
   return redirect(url_for('myAccount'))
  except:
    for row in db.cursor().execute("SELECT rowid, * FROM login ORDER BY rowid"):
     print (str(row))
    logcursor = db.cursor()
    if(request.method=='POST'):
      username = request.form['username']
      password = request.form['password']
      var = db.execute("SELECT password FROM login WHERE username LIKE '"+ username + "'").fetchone()
      var = str(var).replace("(u'", "").replace("',)", "")
      if var == str(password):
        init_sesh(username = username)
        return redirect(url_for('myAccount'))
      else:
        print "password and username don't match"
  return render_template('login.html')


@app.route("/myaccount/")
def myAccount():
  db = get_db()
  try:
    username = session['username']
  except:
    return redirect(url_for('accountlogin'))
  page = ""
  for row in db.execute("SELECT * FROM posts WHERE user LIKE '" +username+"'"):
    page = page + "<div class = 'encapslink'>"
    var1 = row[0]
    var2 = row[1]
    var3 = row[2]
    var4 = row[3]
    html1 = "<a href ='" + var2 + "'>" + var1 + "</a>"
    html2 = "<p>" + var3 + "</p>"
    html3 = "<p>" + var4 + "</p>"
    page = page + html1
    page = page + html2
    page = page + html3
    page = page + "</div>"
  page = Markup(page)


  return render_template('myAccount.html', username = username, page = page)

@app.route("/logout/")
def logout():
  end_sesh()
  return redirect(url_for('accountlogin'))

@app.errorhandler(404)
def page_not_found(error):
  return render_template('error404.html')

if __name__ == "__main__":
  app.run(host='0.0.0.0', debug=True)
